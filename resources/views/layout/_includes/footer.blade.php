<!-- Footer -->
<footer class="page-footer font-small indigo">

<!-- Footer Links -->
<div class="container">
    <!-- Grid row-->
    <div class="row d-flex text-center justify-content-center pt-5">
        <img class="logo-footer" src="<?php echo asset('img/logo.svg')?>">
    </div>
    <!-- Grid row-->
    <div class="row text-center d-flex justify-content-center mb-3">
        <nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar ">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center " id="navbarSupportedContent">
                    <ul class="nav ">
                        <li class="nav-item ">
                            <a id="id_home" class="nav-link scroll active " href="#home">Home</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link scroll product" href="#product">Produto</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link scroll support" href="#support">Suporte</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link plans" href="#plans" data-toggle="modal"
                                data-target="#plansModal">Planos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <hr class="rgba-white-light" style="margin: 0 15%;">
    <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">
    <center style="margin-top: 15px;"><span id="siteseal">
            <script async type="text/javascript"
                src="https://seal.godaddy.com/getSeal?sealID=f585IyGP9ZBpdKdcresuaoUsexOuzPjiTfxasY3Ri4yGjRjRu4ljrt8JzSKf"></script>
            </span></center>
</div>
<!-- Copyright -->
<div class="footer-copyright text-white text-center py-3">© 2019 ALL RIGHTS RESERVED</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->

	
<!-- Large modal -->
@include('main._includes.modal')

<!--plansModal -->
@include('main._includes.modal_plans')

<!-- JavaScript -->
<script src="https://code.jquery.com/jquery-1.11.3.min.js" crossorigin="anonymous"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
crossorigin="anonymous"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
crossorigin="anonymous"></script>
<script src="<?php echo asset('js/all.js')?>"></script>

</body>

</html>