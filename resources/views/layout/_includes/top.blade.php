<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Barlow&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="57x57" href="../img/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../img/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../img/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../img/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../img/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../img/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../img/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../img/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../img/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../img/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../img/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/icon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta http-equiv="Content-Language" content="pt-br, en, es">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo asset('css/all.css')?>">

    <!-- Search Engine -->
    <meta name="description" content="Sucesso Trading é uma empresa especializada em locação de criptomoedas com foco no rendimento de capital com transparência, agilidade e eficácia a todo usuário que utiliza o serviço de locação de nossas moedas (SCN).">
    <meta name="image" content="http://sucessotrading.com/img/logo.svg">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Success Trading">
    <meta itemprop="description" content="Sucesso Trading é uma empresa especializada em locação de criptomoedas com foco no rendimento de capital com transparência, agilidade e eficácia a todo usuário que utiliza o serviço de locação de nossas moedas (SCN).">
    <meta itemprop="image" content="http://sucessotrading.com/img/logo.svg">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Success Trading">
    <meta name="og:description" content="Sucesso Trading é uma empresa especializada em locação de criptomoedas com foco no rendimento de capital com transparência, agilidade e eficácia a todo usuário que utiliza o serviço de locação de nossas moedas (SCN).">
    <meta name="og:image" content="http://sucessotrading.com/img/logo.svg">
    <meta name="og:url" content="http://sucessotrading.com/">
    <meta name="og:site_name" content="Success Trading">
    <meta name="og:locale" content="pt_BR">
    <meta name="og:video" content="http://sucessotrading.com/img/sucesso_es.mp4">
    <meta name="og:type" content="website">

    <title>@yield('title')</title>

</head>

<body>
    <header style = "height: 100%">
        <nav id="id_navbar" class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar ">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-ico">
                        <i class="material-icons">
                            menu
                        </i>
                    </span>
                </button>
                <div class="collapse navbar-collapse justify-content-center " id="navbarSupportedContent">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link home scroll active" href="#home">Home</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link product scroll" href="#product">Produto</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link support scroll" href="#support">Suporte</a>
                        </li> 
                        <!-- <li class="nav-item ">
                            <a class="nav-link" href="#plansModal">Preços</a>
                        </li> -->
                        <li class="nav-item ">
                            <a class="nav-link plans scroll" href="#plans" data-toggle="modal"
                                data-target="#plansModal">Plano de Negócio</a>
                        </li>
                        <li class="nav-item">
                            <a id = "hera" class="login" target="_blank" href="https://sucessotrading.com/hera/?i=pt">
                                <button type="button" class="btn btn-success">Login</button>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="home">
        <div class="col-lg-12 col-md-12 col-sm-12 col-logo">
            <div id="id_detail_logo" class="d-flex flex-column align-items-center box-home">
                <img id="id_logo" class="logo" src="img/logo.svg">
                <h5 id="title">O <strong>sucesso financeiro</strong> <br> nas <strong>suas mãos</strong></h5>
                <br>
                <a href="#plans" data-toggle="modal" data-target="#plansModal">
                    <button id="btnPlans" type="button" class="btn btn-success btn-lg">
                        Conheça aqui nossos planos
                    </button>
                </a>
            </div>
            </div>
        </div>
        <!--<div class="col-lg-6 col-md-6 col-sm-12 col-register-user">
            
        </div> -->       
</div>
    </header>