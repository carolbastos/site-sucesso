<h1>Message Received</h1>

<ul>
    <li class="">Name: {{ $name }}</li>
    <li class="">Email: {{ $email }}</li>
    <li class="">Subject: {{ $subject }}</li>
    <li class="">Message: {{ $bodyMessage }}</li>
</ul>