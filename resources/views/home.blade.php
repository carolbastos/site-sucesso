@extends('layout.template')

@section('title','Sucesso Trading')

@section('content') 
<!--Main Layout-->
<div id="home" class="bg-main">
  @include('main._includes.home')
    <main>
      <!---about-->
        @include('main._includes.about')
      <!---scn-->
    </main>
    @include('main._includes.scn')
      <!---contact-->
      @include('main._includes.contact')
</div>
<!---end bg-main-->    
@endsection