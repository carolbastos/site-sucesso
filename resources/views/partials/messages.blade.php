
@if(count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Errors</strong>
        <ul>
            @foreach($errors->all() as $erros)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif