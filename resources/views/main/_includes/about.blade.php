<div class="container">
<div class="about">
                    <div id="about" class="row">
                        <div class="col-lg-6 col-md-6">
                            <hr>
                            <h3>SOBRE</h3>
                            <h2>NÓS</h2>
                            <p id="text-about">Sucesso Trading é uma empresa especializada em locação de criptomoedas
                                com foco no rendimento de capital com transparência, agilidade e eficácia a todo usuário
                                que utiliza o serviço de locação de nossas moedas (SCN).</p>

                        </div>
                        <div class="col-lg-6 col-md-6 coin">
                            <img class="img-fluid" src="img/03-moeda.png">
                        </div>
                    </div>

                    <div class="row cards-about">
                        <div class="cols-about col-lg-4 col-md-4 col-sm-12 col-12">
                            <div id="mission" class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <img src="img/03-missao.png" class="align-self-end mr-3 card-img-top"
                                                alt="Missão - Sucesso">
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10">
                                            <h5 class="card-title">MISSÃO</h5>
                                        </div>
                                    </div>

                                    <p class="card-text">Oferecer com excelência aos nossos clientes o
                                        serviço de trade e auxiliá-los a alcançar o sucesso financeiro, de maneira
                                        íntegra,
                                        efetiva e
                                        progressiva.</p>
                                </div>
                            </div>
                        </div>
                        <div class="cols-about col-lg-4 col-md-4 col-sm-12 col-12">
                            <div id="view" class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <img src="img/03-visao.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10">
                                            <h5 class="card-title">VISÃO</h5>
                                        </div>
                                    </div>
                                    <p class="card-text">Oferecer com excelência aos nossos clientes o serviço de trade
                                        e
                                        auxiliá-los a alcançar o sucesso financeiro, de maneira íntegra, efetiva e
                                        progressiva.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="cols-about col-lg-4 col-md-4 col-sm-12 col-12">
                            <div id="values" class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                            <img src="img/03-valores.png" class="card-img-top" alt="...">
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10">
                                            <h5 class="card-title">VALORES</h5>
                                        </div>
                                    </div>
                                    <p class="card-text">Transparência; Segurança para o cliente; Prosperidade; Sucesso
                                        financeiro
                                        Tecnologia; O novo tesouro Simplicidade; Desburocratizando o mercado financeiro
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>
