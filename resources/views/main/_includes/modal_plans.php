<div class="modal fade" id="plansModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
    <div class="modal-dialog plans-dialog" role="document">
        <div id="modal-plans" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> 
            </div>
            <div class="modal-body">
                <h2 id="greatp" class="modal-title great-profit" >Ótimos rendimentos com criptomoedas</h2>
                <p id="greatd" class="weekly-income">Tenha um rendimento semanal de 3% e anual de 156%</p>
                <img id="img-prices" src="img/pt/rendimentos.png">
            </div>
        </div>
    </div>
</div>