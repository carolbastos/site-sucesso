<div id="support" class="container center-block container-contact">
    <div class="row pt-5">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="title-contact">
                <hr>
                <h3>ENTRE EM</h3>
                <h2>CONTATO</h2>
                <ul class="info-contact pt-2"> 
                    <li>
                        <a href="https://www.instagram.com/sucessotrading/" target="_blank"><img class="img-fluid mr-2" src="/img/icons/insta.png">@sucessotrading</a>
                    </li>
                    <li>
                        <a href="https://api.whatsapp.com/send?phone=5592991049702&text=Ola%20gra%C3%A7as%20pelo%20seu%20contato%20como%20posso%20le%20ajudar" target="_blank">
                        <img class="img-fluid mr-2" src="/img/icons/wpp.png">+55 92 9204-9702</a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/sucessotradingusa/" target="_blank"><img class="img-fluid mr-2" src="/img/icons/insta.png">@sucessotradingusa</a>
                    </li>
                    <li>
                        <a href="https://api.whatsapp.com/send?phone=13055193817&text=En%20que%20le%20podemos%20servir%20" target="_blank">
                        <img class="img-fluid mr-2" src="/img/icons/wpp.png">+1 (305) 519-3817</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/sucessotrading/" target="_blank"><img class="img-fluid mr-2" src="/img/icons/fb.png">Sucesso Trading</a>
                    </li>
                </ul> 
            </div>
        </div>
            <div class="col-lg-6 col-md-6 col-sm-12 contact">
                <div class="row">
                    <div class="col-form">
                        <form action="{{ route('contact.send')}}" method="POST">
                            @include('partials.messages')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" id="inputName" aria-describedby="emailHelp"
                                    placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp"
                                    placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input name="subject" type="text" class="form-control" id="inputSubject" placeholder="Assunto">
                            </div>
                            <div class="form-group">
                                <textarea name="bodyMessage" id="textarea" class="form-control" placeholder="Sua mensagem"
                                    rows="3"></textarea>
                            </div>
                            <!-- <div class="form-group">
                               <div class="col-md-6 offset-md-4">
                                   <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                                    @if($errors->has('g-recaptcha-response'))
                                    <span class="invalid-feedback" style="display:block">
                                        <strong>{{$errors->first('g-recaptcha-response')}}</strong>    
                                    </span>
                                    @endif
                                </div>
                            </div> -->
                            <div class="text-center div_btn-contact">
                                <button type="submit" class="btn">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            </div>
        </div>
        <script src="https://www.google.com/recaptcha/api.js?render=_reCAPTCHA_site_key"></script>
