<div class="text-left scn" id="product">
                <div class="container">
                    <div class="row">
                        <div class="col-video col-lg-6 col-md-6 col-md-12">
                            <video id="videoSucesso" controls>
                                <source id="mp4_src" src="img/sucesso_pt.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-coin col-lg-6 col-md-6 col-md-12">
                            <hr>
                            <div class="pl-2 pt-1">
                                <h3>CRIPTOMOEDA PRÓPRIA</h3>
                                <h1>SUCESSO COIN - SCN</h1>
                                <p class="text-left scn-detail">
                                    SCN é uma criptomoeda nova, criada pela Sucesso Trading com a intenção de
                                    facilitar ainda mais as suas transações em operações descentralizadas,
                                    utilizando a tecnologia de segurança Blockchain, ou seja, tornando-as
                                    ágeis e menos sujeitas a golpes e fraudes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
