<!-- Large modal -->
<div class="modal " id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content text-center">
        <h3 id="selectLanguage" class="font-weight-bold">Escolha um idioma</h3>
        <div class="row animated slideInUp text-center row-languages">
            <div id="pt" class="col-lg-4 col-md-12 col-sm-12 ">
                <div class="card">
                    <a>
                        <img class="card-img-top" src="<?php echo asset('img/01-BandeiraBrasil.png')?>" alt="Bandeira - Brasil">
                        <h5 class="card-title">Português</h5>
                    </a>
                </div>
            </div>
            <div id="en" class="col-lg-4 col-md-12 col-sm-12 ">
                <div class="card">
                    <a>
                        <img class="card-img-top" src="<?php echo asset('img/01-BandeiraEUA.png')?>" alt="Bandeira - EUA">
                        <h5 class="card-title">English</h5>
                    </a>
                </div>
            </div>
            <div id="es" class="col-lg-4 col-md-12 col-sm-12 ">
                <div class="card">
                    <a>
                        <img class="card-img-top" src="<?php echo asset('img/02-BandeiraEspanha.png')?>" alt="Bandeira - Espanha">
                        <h5 class="card-title">Español</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
