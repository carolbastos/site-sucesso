<div id="id-form-create-user" class="form-create-user">
    <div class="border-left-cad">
        <h3 id="register-text" class="text-cadastre-se">Cadastre-se agora</h3>
        <p id="register-description">E converse com um dos nossos especialistas em Manaus:</p>
    </div>
    
                <form action="{{ route('users.store') }}" method="POST" 
                    <!-- @include('partials.messages') -->
                    @csrf <!-- {{ csrf_field() }} -->
                    @method('POST')
                    <div class="form-group">
                        <label id="name-user">Nome:</label>
                        <input name="name" type="text" class="form-control" aria-describedby="aName">
                    </div>
                    <div class="form-group">
                        <label id="phone-user">Seu celular:</label>
                        <div class="media">
                            <div class="media-body">
                                <input name="phone" type="number" class="form-control" aria-describedby="aPhone">
                            </div>
                        </div>
                        
                         </div>
                    <div class="form-group">
                        <label id="email-user">Email:</label>
                        <input name="email" type="email" class="form-control" aria-describedby="aEmail">
                    </div>

                    <div class="text-right btnRegister">
                        <button  type="submit" class="btn btn-success btn-lg">
                            Register
                        </button>
                    </div>
                </form>
            </div>