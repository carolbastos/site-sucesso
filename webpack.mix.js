const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/* mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css'); */

//Task to compile Less
mix.less('resources/less/styles.less', 'public/css')
    .options({
        processCssUrls: false
    });

//Task to concatenate CSS
mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/styles.css',
    'public/css/animate.min.css'    
], 'public/css/all.css');

//Task to concatenate JS
mix.scripts([
    'public/js/main.js',
    'public/js/translator.js'
], 'public/js/all.js');

/* mix.less('resources/less/styles.less', 'public/css'); */
