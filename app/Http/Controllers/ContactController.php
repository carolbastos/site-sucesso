<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    //
    public function postContact(Request $request){
        /* dd($request); */
        $fields = $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|email',
            'subject' => 'required|max:50',
            'bodyMessage' => 'required'
        ]);
        Mail::to('stac.usa@sucessotrading.com')->send(new ContactMail($fields));
        
        return back()->withInput();
      /*   return redirect()->route('/'); */
    }
}
