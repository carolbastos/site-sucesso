const cardPt = document.getElementById('pt');
        const cardEn = document.getElementById('en');
        const cardEs = document.getElementById('es');
        const selectLanguage = document.getElementById('selectLanguage');

        $(function () {
            $('#myModal').modal('show');
            clicked();
        })

        function resolveAfterSeconds() {
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                }, 3000);
            });
        }

        async function closeModal() {
            await resolveAfterSeconds();
            $('#myModal').modal('hide');
            animateHome();
        }

        function clicked() {

            $("#pt").on("click", function () {
                animatePt();
                closeModal();
            });

            $("#en").on("click", function () {
                animateEn();
                closeModal();
            });

            $("#es").on("click", function () {
                animateEs();
                closeModal();
            });

        }

        function animatePt() {
            $('#pt').each(function (i) {
                cardPt.classList.add('animated', 'fadeOutRight', 'delay-1s');
                selectLanguage.classList.add('animated', 'fadeOutDown', 'delay-2s');
                cardEn.classList.add('animated', 'fadeOutDown');
                cardEs.classList.add('animated', 'fadeOutDown');
            });
        }

        function animateEn() {
            $('#en').each(function (i) {
                cardEn.classList.add('animated', 'fadeOut', 'delay-1s');
                selectLanguage.classList.add('animated', 'fadeOutDown', 'delay-1s');
                cardPt.classList.add('animated', 'fadeOutDown');
                cardEs.classList.add('animated', 'fadeOutDown');
                $(window).on("load", loadTranslation("en"));
            });
        }

        function animateEs() {
            $('#es').each(function (i) {
                cardEs.classList.add('animated', 'fadeOutLeft', 'delay-1s');
                selectLanguage.classList.add('animated', 'fadeOutDown', 'delay-2s');
                cardPt.classList.add('animated', 'fadeOutDown');
                cardEn.classList.add('animated', 'fadeOutDown');
                $(window).on("load", loadTranslation("es"));
            });
        }

        function animateHome() {
            const id_navbar = document.getElementById('id_navbar');
            const id_detail_logo = document.getElementById('id_detail_logo');
            const id_logo = document.getElementById('id_logo');
            const id_form_create_user = document.getElementById('id-form-create-user');
            id_navbar.classList.add('animated', 'fadeInUp', 'delay-2s');
            id_detail_logo.classList.add('animated', 'fadeInUp', 'delay-1s');
            //id_form_create_user.classList.add('animated', 'fadeInUp', 'delay-1s');
            id_logo.classList.add('animated', 'fadeInUp');

            
        }

        /* SCRIPT JQUERY PARA TROCAR DE COR NAVBAR */
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#id_navbar').css("background-color", "#FFF")
                $('#id_navbar ul li a ').css("color", "black")
                $('#id_navbar').css("top", "0")
                $('.material-icons').css("color", "#28a745")
            } else {
                $('#id_navbar').css("background-color", "transparent")
                $('#id_navbar ul li a ').css("color", "white")
                $('#id_navbar').css("top", "24px")
            }
        });

        /*SCROLL SUAVE */
        var $doc = $('html, body');
        $('.scroll').click(function() {
            $doc.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });