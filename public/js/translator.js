var title = '';
var plano = ''
var btnPlans = '';
var navbar = '';
var product = '';
var support = '';
var plans = '';
var about, about_text = '';
var mission, view, values = '';
var onw = '';
var success, scnText, contact, btn = '';
var titleCards = '';
var prices = '';
var register = '';

var translation = [];

function loadTranslation(language) {

    if (language == "en") {
        $("#hera").attr("href", "https://sucessotrading.com/hera/?i=en")
        $.getJSON("json/en.json", function (data) {
            replacesHtml(data);
            videoFunction();
        });
    }
    else if (language == "es") {
        $("#hera").attr("href", "https://sucessotrading.com/hera/?i=es")
        $.getJSON("json/es.json", function (data) {
            replacesHtml(data);
            videoFunction();
        });
    }

    $("html").attr("lang", language);

}

function replacesHtml(data) {

    translation = data.translation;

    for (i = 0; i < translation.length; i++) {

        title += translation[i].title;
        btnPlans += translation[i].btnPlans;
        //navbar
        navbar += translation[i].navbar;
        plano += translation[i].plano;
        //about
        about = translation[i].about[0];
        about_text = translation[i].about[1];
        mission = translation[i].mission;
        view = translation[i].view;
        values = translation[i].values;
        titleCards = translation[i].titleCards;
        //scn
        onw = translation[i].onw;
        success = translation[i].success;
        scnText = translation[i].scnText;
        //contact
        contact = translation[i].contact;
        btn = translation[i].btn;
        //prices
        prices = translation[i].prices;
        register = translation[i].register;
    }

    //replace home
    document.getElementById('title').innerHTML = title;
    document.getElementById('btnPlans').innerHTML = btnPlans;

    //replace navbar 
    var nav = navbar.split(" ");
    $('.navbar .product').text(nav[0]);
    $('.navbar .support').text(nav[1]);
    $('.navbar .plans').text(plano);
   
    //replace about
    var about_us = about.split(" ");
    $('.about h3').text(about_us[0]);
    $('.about h2').text(about_us[1]);
    $('.about p').text(about_text);
    $('.cards-about #mission p').text(mission); 
    $('.cards-about #view p').text(view); 
    $('.cards-about #values p').text(values); 

    var titleCard = titleCards.split(" ");
    $('#mission h5').text(titleCard[0]);
    $('#view h5').text(titleCard[1]);
    $('#values h5').text(titleCard[2]);

    //replace scn
    $('.scn h3').text(onw);
    $('.scn h1').text(success);
    $('.scn p').text(scnText);

    //replace contact
    
    var contact_us = contact[0].split(" ");

    $('.title-contact h3').text(contact_us[0]);
    $('.title-contact h2').text(contact_us[1]);
    document.getElementById("inputName").placeholder = contact[1];
    document.getElementById("inputEmail").placeholder = contact[2];
    document.getElementById("inputSubject").placeholder = contact[3];
    document.getElementById("textarea").placeholder = contact[4];
    $('.contact .btn').text(btn);

    //replace register
    /*document.getElementById('register-text').innerHTML = register[0];
    document.getElementById('register-description').innerHTML = register[1];
    document.getElementById('name-user').innerHTML = register[2];
    document.getElementById('phone-user').innerHTML = register[3];*/
    // document.getElementById('greatd').innerHTML = prices[1];

    //replace prices
    debugger;
    document.getElementById('greatp').innerHTML = prices[0];
    document.getElementById('greatd').innerHTML = prices[1];
    var imgPrices = document.getElementById("img-prices");
    imgPrices.src = prices[3];
}

//replace video translate
var vid = document.getElementById("videoSucesso");

function videoFunction() { 
    vid.src = "img/sucesso_es.mp4";
    vid.load();
  } 